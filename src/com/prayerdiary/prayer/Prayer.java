package com.prayerdiary.prayer;

public class Prayer {
    public String name;
    public String text;
    
    public Prayer(String name, String text) {
    	this.name = name;
    	this.text = text;
    }
    
    public static Prayer getInstance(String prayer, String language) {
    	if (!"en".equals(language)) {
    		return null;
    	}
    	if ("OUR_FATHER".equals(prayer)) {
    		return new Prayer("Our Father", "Our Father, who art in heaven, hallowed be Thy name: Thy kingdom come: Thy will be done on earth as it is in heaven. Give us this day our daily bread: and forgive us our trespasses as we forgive those who trespass against us. And lead us not into temptation: but deliver us from evil. Amen.");
    	}
    	if ("FATIMA".equals(prayer)) {
    		return new Prayer("The Fatima Prayer", "O my Jesus, forgive us our sins, save us from the fires of hell, and lead all souls to Heaven, especially those in most need of Your Mercy");
    	}
    	if ("ROSARY_SorM1".equals(prayer)) {
    		return new Prayer("The Agony of Jesus in the garden: Hail Mary", "Hail Mary, full of grace, the Lord is with thee: blessed art thou among women, and blessed is the fruit of thy womb, Jesus. Holy Mary, Mother of God, pray for us sinners, now and at the hour of our death. Amen");
    	}
    	
    	if ("GLORY_BE".equals(prayer)) {
    		return new Prayer("Glory Be", "Glory be to the Father, and to the Son and to the Holy Spirit. As it was in the beginning, is now and ever shall be, world without end. Amen.");
    	}
    	
    	return null;
    }
    
    public static Prayer getInstance(String prayer) {
    	return getInstance(prayer, "en");
    }

}
