package com.prayerdiary.prayer;

public class PrayerSetEntry {
    private Prayer prayer;
    private int repetitions;
    private int currentRepetition;
    
    private PrayerSetEntry() {
    	currentRepetition = 1;
    }
    
    protected int getRepetitions() {
    	return repetitions;
    }
    
    protected int getCurrentRepetition() {
    	return currentRepetition;
    }
    
    protected static PrayerSetEntry getInstance(Prayer prayer, int repetitions) {
    	PrayerSetEntry entry = new PrayerSetEntry();
    	entry.prayer = prayer;
    	entry.repetitions = repetitions;
    	
    	return entry;
    }
    
    public boolean isFinished() {
    	return currentRepetition >= repetitions;
    }
    
    protected Prayer getPrayer() {
    	return prayer;
    }
    
    protected void next() {
    	currentRepetition++;
    }

}
