package com.prayerdiary.prayer;

import java.util.ArrayList;
import java.util.List;

public class PrayerSet {
	private int totalPrayers = 0;
	
	private int currentPrayerCounter = 0;
	
    private List<PrayerSetEntry> prayers = new ArrayList<PrayerSetEntry> ();

    private int currentPrayerIndex;
    
    private PrayerSet(List<PrayerSetEntry> prayers) {
    	this.prayers = prayers;
    	
    	currentPrayerCounter = 0;
    	currentPrayerIndex = 0;
    	
    	countTotalPrayers();
    }
    
    private PrayerSet() {
    	this.prayers = new ArrayList<PrayerSetEntry>();
    	
    	currentPrayerCounter = 0;
    	currentPrayerIndex = 0;
    	
    	countTotalPrayers();
    }
    
    private void addPrayer(Prayer prayer, int repetitions) {
    	
    	prayers.add(PrayerSetEntry.getInstance(prayer, repetitions));
    	
    	totalPrayers += repetitions;
    }
    
    private void countTotalPrayers() {
    	for (PrayerSetEntry entry: prayers) {
    		totalPrayers += entry.getRepetitions();
    	}
    }

    public int getProgress() {
    	if (isFinished()) {
    		return 100;
    	}
    	return (currentPrayerCounter + 1) * 100 / totalPrayers;
//    	return currentPrayerCounter + "/" + totalPrayers;
    }
    
    private boolean isCurrentPrayerFinished() {
    	return prayers.get(currentPrayerIndex).isFinished();
    }

    public boolean isFinished() {
    	return currentPrayerIndex > prayers.size() - 1 || (currentPrayerIndex == prayers.size() - 1 
    			&& isCurrentPrayerFinished());
    }
    
    public Prayer getCurrentPrayer() {
    	if (isFinished()) {
    		return prayers.get(prayers.size() - 1).getPrayer();
    	}
    	return prayers.get(currentPrayerIndex).getPrayer();
    }
    
    private PrayerSetEntry getCurrentPrayerSetEntry() {
    	return prayers.get(currentPrayerIndex);
    }

    public void next() {
    	if (isFinished()) {
    		//TODO: throw exception here
    	}
    	else {
    		if (isCurrentPrayerFinished()) {
    		currentPrayerIndex++;
    	}
    	else {
    		getCurrentPrayerSetEntry().next();
    	}
    	}
    	currentPrayerCounter++;
    }

    public static void getInstance() {
    	
    }
    
    //TODO: it exists for test purposes only; right now, just sorrowful mysteries, first mystery
    public static PrayerSet getRosaryPrayer() {
    	PrayerSet rosary = new PrayerSet();
    	
    	Prayer ourfather = Prayer.getInstance("OUR_FATHER");
    	
    	Prayer fatima = Prayer.getInstance("FATIMA");
    	
    	Prayer glory = Prayer.getInstance("GLORY_BE");
    	
    	Prayer mystery1 = Prayer.getInstance("ROSARY_SorM1");
    	
    	rosary.addPrayer(ourfather, 1);
    	rosary.addPrayer(mystery1, 10);
    	rosary.addPrayer(glory, 1);
    	rosary.addPrayer(fatima, 1);

    	return rosary;
    }

}
