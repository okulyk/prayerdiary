package com.prayerdiary;

import com.prayerdiary.prayer.PrayerSet;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.os.Build;

public class PrayingActivity extends ActionBarActivity {
	PrayerSet prayerSet;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		prayerSet = PrayerSet.getRosaryPrayer();
		
		setContentView(R.layout.activity_praying);

//		if (savedInstanceState == null) {
//			getSupportFragmentManager().beginTransaction()
//					.add(R.id.container, new PlaceholderFragment()).commit();
//		}
		
		updateView();
	}
	
	private String currentText() {
		String s = "";
		s += prayerSet.getCurrentPrayer().text + "\n";
		
		ProgressBar mProgress = (ProgressBar) findViewById(R.id.progressBar);
		mProgress.setProgress(prayerSet.getProgress());

		return s;
	}
	
	private void updateView() {
		TextView tx = (TextView) findViewById(R.id.prayertext);
		tx.setText(currentText());
	}
	
	public void onButtonClick(View view) {
	    prayerSet.next();
	    updateView();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.praying, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
